package com.travel.service;

import com.travel.dao.ScenicSpotDao;
import com.travel.dao.TourRouteDao;
import com.travel.entity.ScenicSpot;
import com.travel.entity.TourRoute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Spliterator;

@Service
public class TourRouteService {
    @Autowired
    TourRouteDao tourRouteDao;

    @Autowired
    ScenicSpotDao scenicSpotDao;

    public List<TourRoute> getAll(){
        return tourRouteDao.getAll();
    }

    public TourRoute getOne(Long trId){
        TourRoute tourRoute = tourRouteDao.getOne(trId);
        List<ScenicSpot> spotList = scenicSpotDao.getByTrId(trId);
        for (ScenicSpot spot:spotList){
            tourRoute.getSpotList().add(spot.getSsId());
        }
        return tourRoute;
    }

    @Transactional
    public void add(TourRoute tourRoute){
        tourRoute.setCreateTime(new Date());
        tourRouteDao.add(tourRoute);
        for (Long ssId: tourRoute.getSpotList()) {
            tourRouteDao.addTourScenicRelation(tourRoute.getTrId(),ssId);
        }
    }

    @Transactional
    public void delete(Long trId){
        tourRouteDao.deleteTourScenicRelation(trId);
        tourRouteDao.delete(trId);
    }

    @Transactional
    public void update(TourRoute tourRoute){
        tourRouteDao.update(tourRoute);
        //删除旧景点关系
        tourRouteDao.deleteTourScenicRelation(tourRoute.getTrId());
        //添加新景点关系
        for (Long ssId: tourRoute.getSpotList()) {
            tourRouteDao.addTourScenicRelation(tourRoute.getTrId(),ssId);
        }
    }
}
