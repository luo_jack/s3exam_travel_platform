package com.travel.service;

import com.travel.dao.ScenicSpotDao;
import com.travel.entity.ScenicSpot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class ScenicSpotService {

    @Autowired
    ScenicSpotDao scenicSpotDao;

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    public List<ScenicSpot> getAll(){
        List<ScenicSpot> spotList = null;
        if(redisTemplate.hasKey("spotList")){
            spotList = (List<ScenicSpot>) redisTemplate.opsForValue().get("spotList");
        }else{
            spotList = scenicSpotDao.getAll();
            redisTemplate.opsForValue().set("spotList",spotList,1, TimeUnit.DAYS);
        }
        return spotList;
    }

    //根据路由路线拿
    public List<ScenicSpot> getByTrId(Long trId){
        return scenicSpotDao.getByTrId(trId);
    }
}
