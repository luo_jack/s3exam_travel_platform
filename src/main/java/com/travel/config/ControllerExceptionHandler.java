package com.travel.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

//全局异常处理
@ControllerAdvice
@Slf4j
public class ControllerExceptionHandler {

    @ExceptionHandler(value = RuntimeException.class)
    @ResponseBody
    public ResponseEntity exceptionHandler(RuntimeException ex){
        log.warn("一般异常,"+ex.getMessage());
        return ResponseEntity.error(ex.getMessage());
    }
}
