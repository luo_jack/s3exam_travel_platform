package com.travel.config;

import lombok.Data;

@Data
public class ResponseEntity {

    private boolean success = true;

    private Object data;

    private String message;

    public static ResponseEntity success(Object data){
        ResponseEntity resp = new ResponseEntity();
        resp.setSuccess(true);
        resp.setData(data);
        return resp;
    }

    public static ResponseEntity success(){
        ResponseEntity resp = new ResponseEntity();
        resp.setSuccess(true);
        return resp;
    }

    public static ResponseEntity error(String message){
        ResponseEntity resp = new ResponseEntity();
        resp.setSuccess(false);
        resp.setMessage(message);
        return resp;
    }
}
