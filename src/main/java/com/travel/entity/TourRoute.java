package com.travel.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//路线
public class TourRoute {

    private Long trId;

    private String trName;

    private Integer trType;

    private Integer trPrice;

    private String trPhone;

    private String trUser;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    //线路包含景点
    private List<Long> spotList = new ArrayList<>();

    public Long getTrId() {
        return trId;
    }

    public void setTrId(Long trId) {
        this.trId = trId;
    }

    public String getTrName() {
        return trName;
    }

    public void setTrName(String trName) {
        this.trName = trName;
    }

    public Integer getTrType() {
        return trType;
    }

    public void setTrType(Integer trType) {
        this.trType = trType;
    }

    public Integer getTrPrice() {
        return trPrice;
    }

    public void setTrPrice(Integer trPrice) {
        this.trPrice = trPrice;
    }

    public String getTrPhone() {
        return trPhone;
    }

    public void setTrPhone(String trPhone) {
        this.trPhone = trPhone;
    }

    public String getTrUser() {
        return trUser;
    }

    public void setTrUser(String trUser) {
        this.trUser = trUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public List<Long> getSpotList() {
        return spotList;
    }

    public void setSpotList(List<Long> spotList) {
        this.spotList = spotList;
    }
}
