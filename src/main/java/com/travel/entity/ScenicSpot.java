package com.travel.entity;

//景点
public class ScenicSpot {

    private Long ssId;

    private String ssName;

    private String ssDuration;

    private String ssContent;

    public Long getSsId() {
        return ssId;
    }

    public void setSsId(Long ssId) {
        this.ssId = ssId;
    }

    public String getSsName() {
        return ssName;
    }

    public void setSsName(String ssName) {
        this.ssName = ssName;
    }

    public String getSsDuration() {
        return ssDuration;
    }

    public void setSsDuration(String ssDuration) {
        this.ssDuration = ssDuration;
    }

    public String getSsContent() {
        return ssContent;
    }

    public void setSsContent(String ssContent) {
        this.ssContent = ssContent;
    }
}
