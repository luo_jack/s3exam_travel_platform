package com.travel.dao;

import com.travel.entity.TourRoute;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface TourRouteDao {

    @Select("select * from tour_route")
    List<TourRoute> getAll();

    @Select("select * from tour_route where tr_id=#{trId}")
    TourRoute getOne(Long trId);

    @Insert("INSERT INTO `exam`.`tour_route` (\n" +
            "  `tr_name`,`tr_type`,`tr_price`,`tr_phone`,`tr_user`,`create_time`\n" +
            ") \n" +
            "VALUES\n" +
            "  (\n" +
            "    #{trName},#{trType},#{trPrice},#{trPhone},#{trUser},#{createTime}\n" +
            "  ) ;")
    @Options(useGeneratedKeys = true,keyProperty = "trId",keyColumn = "tr_id")
    void add(TourRoute tourRoute);

    @Insert("INSERT INTO `exam`.`tour_scenic_relation` (`ss_id`, `tr_id`) \n" +
            "VALUES\n" +
            "  (#{ssId}, #{trId}) ;")
    void addTourScenicRelation(@Param("trId") Long trId,@Param("ssId") Long ssId);

    @Delete("delete from tour_route where tr_id=#{trId}")
    void delete(Long trId);

    @Delete("delete from tour_scenic_relation where tr_id=#{trId}")
    void deleteTourScenicRelation(Long trId);

    @Update("UPDATE \n" +
            "  `exam`.`tour_route` \n" +
            "SET\n" +
            "  `tr_name` = #{trName},\n" +
            "  `tr_type` = #{trType},\n" +
            "  `tr_price` = #{trPrice},\n" +
            "  `tr_phone` = #{trPhone},\n" +
            "  `tr_user` = #{trUser} \n" +
            "WHERE `tr_id` = #{trId} ;")
    void update(TourRoute tourRoute);
}
