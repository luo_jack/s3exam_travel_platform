package com.travel.dao;

import com.travel.entity.ScenicSpot;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ScenicSpotDao {

    @Select("select * from scenic_spot")
    List<ScenicSpot> getAll();

    //根据路由路线拿
    @Select("SELECT t1.* FROM scenic_spot t1\n" +
            "INNER JOIN tour_scenic_relation t2\n" +
            "ON t1.`ss_id`=t2.`ss_id`\n" +
            "WHERE t2.`tr_id`=#{trId}")
    List<ScenicSpot> getByTrId(Long trId);
}
