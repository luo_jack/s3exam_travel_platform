package com.travel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TravelPlatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(TravelPlatformApplication.class,args);
    }

    @GetMapping("/hello")
    public String hello(){
        return "hello,world";
    }

    @Bean
    public RedisTemplate<String, Object> jdkRedisTemplate(RedisConnectionFactory factory){
        RedisTemplate<String, Object> redis=new RedisTemplate<>();
        redis.setConnectionFactory(factory);
        redis.setKeySerializer(RedisSerializer.string());
        redis.setValueSerializer(RedisSerializer.json());
        return redis;
    }
}
