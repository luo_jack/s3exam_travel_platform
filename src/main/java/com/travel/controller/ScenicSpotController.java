package com.travel.controller;

import com.travel.config.ResponseEntity;
import com.travel.service.ScenicSpotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/spot")
public class ScenicSpotController {

    @Autowired
    ScenicSpotService scenicSpotService;

    @GetMapping("/all")
    public ResponseEntity getAll() {
        return ResponseEntity.success(scenicSpotService.getAll());
    }

    @GetMapping("/list")
    public ResponseEntity getByTrId(Long trId){
        return ResponseEntity.success(scenicSpotService.getByTrId(trId));
    }
}
