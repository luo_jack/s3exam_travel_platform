package com.travel.controller;

import com.travel.config.ResponseEntity;
import com.travel.entity.TourRoute;
import com.travel.service.TourRouteService;
import jdk.nashorn.internal.ir.RuntimeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/route")
public class TourRouteController {

    @Autowired
    TourRouteService tourRouteService;

    @GetMapping("/all")
    public ResponseEntity getAll(){
        return ResponseEntity.success(tourRouteService.getAll());
    }

    @GetMapping("/one")
    public ResponseEntity getOne(Long trId){
        return ResponseEntity.success(tourRouteService.getOne(trId));
    }

    @PostMapping("/add")
    public ResponseEntity add(@RequestBody TourRoute tourRoute){
        tourRouteService.add(tourRoute);
        return ResponseEntity.success();
    }

    @PostMapping("/delete")
    public ResponseEntity delete(Long trId){
        tourRouteService.delete(trId);
        return ResponseEntity.success();
    }

    @PostMapping("/update")
    public ResponseEntity update(@RequestBody TourRoute tourRoute){
        tourRouteService.update(tourRoute);
        return ResponseEntity.success();
    }
}
