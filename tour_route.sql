/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.7.17-log : Database - exam
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`exam` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `exam`;

/*Table structure for table `scenic_spot` */

DROP TABLE IF EXISTS `scenic_spot`;

CREATE TABLE `scenic_spot` (
  `ss_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ss_name` varchar(200) NOT NULL,
  `ss_duration` varchar(200) NOT NULL,
  `ss_content` varchar(200) NOT NULL,
  PRIMARY KEY (`ss_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `scenic_spot` */

insert  into `scenic_spot`(`ss_id`,`ss_name`,`ss_duration`,`ss_content`) values (1,'地点1','0.5天','浏览1'),(2,'地点2','1天','浏览2'),(3,'地点3','2天','浏览3'),(4,'地点4','2小时','浏览4');

/*Table structure for table `tour_route` */

DROP TABLE IF EXISTS `tour_route`;

CREATE TABLE `tour_route` (
  `tr_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tr_name` varchar(200) NOT NULL,
  `tr_type` int(11) NOT NULL,
  `tr_price` int(11) NOT NULL,
  `tr_phone` varchar(200) NOT NULL,
  `tr_user` varchar(200) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`tr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `tour_route` */

insert  into `tour_route`(`tr_id`,`tr_name`,`tr_type`,`tr_price`,`tr_phone`,`tr_user`,`create_time`) values (1,'线路1',1,100,'1234','小张','2023-03-30 11:45:46'),(2,'线路1',2,200,'3456','小李','2023-04-01 08:45:38'),(3,'测试路线',1,358,'7890','小红','2023-04-01 10:44:56');

/*Table structure for table `tour_scenic_relation` */

DROP TABLE IF EXISTS `tour_scenic_relation`;

CREATE TABLE `tour_scenic_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ss_id` bigint(20) DEFAULT NULL,
  `tr_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ss` (`ss_id`),
  KEY `fk_tr` (`tr_id`),
  CONSTRAINT `fk_ss` FOREIGN KEY (`ss_id`) REFERENCES `scenic_spot` (`ss_id`),
  CONSTRAINT `fk_tr` FOREIGN KEY (`tr_id`) REFERENCES `tour_route` (`tr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `tour_scenic_relation` */

insert  into `tour_scenic_relation`(`id`,`ss_id`,`tr_id`) values (2,1,1),(3,3,1),(4,1,2),(5,2,2),(6,4,2),(7,2,3),(8,3,3);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
